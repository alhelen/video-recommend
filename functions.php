<?php
session_start();

function readData() {
    if (!isset($_SESSION['data'])) {
        $file = fopen("data/videos.csv","r");
        $data = array();

        while(! feof($file))
        {
            array_push($data, fgetcsv($file));
        }

        fclose($file);
        $_SESSION['data'] = $data;
    }
}

function splitURL($givenurl) {
    return array("https://img.youtube.com/vi/" . explode("/",$givenurl[1])[3] . "/0.jpg", $givenurl[0], str_replace("]", "",str_replace("[", "", $givenurl[2])));
}

function getVideos() {
    $data = $_SESSION['data'];
    $size = sizeof($data);
    return array(splitURL($data[rand(0, $size-1)]), splitURL($data[rand(0, $size-1)]), splitURL($data[rand(0, $size-1)]), splitURL($data[rand(0, $size-1)]), splitURL($data[rand(0, $size-1)]), splitURL($data[rand(0, $size-1)]));

}


?>