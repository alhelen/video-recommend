<?php

session_start();

?>
<!DOCTYPE html>
<html>
<style>
* {
    box-sizing: border-box;
}

html {
    height: 100%;
    width: 100%;
    margin: 0;
    font-family: Arial;
}

body {
    height: 100%;
    width: 100%;
    margin: 0;
    font-family: Arial;
}

.header {
    text-align: center;
    padding: 32px;
}

.row {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    -ms-flex-wrap: wrap; /* IE10 */
    flex-wrap: wrap;
    padding: 0 4px;
    height: 100%;
}

.column {
    -ms-flex: 33%; /* IE10 */
    flex: 33%;
    max-width: 33%;
    padding: 0 4px;
}

.column img {
    max-height: 50%;
    position: relative;
    margin-top: 8px;
    vertical-align: middle;
}

.first {
    position: absolute;
    top: 0%;
    left: 3%;
    font-size:2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
    background-color: grey;
    width: 350px;
}

.second {
    position: absolute;
    top: 0%;
    left: 35%;
    z-index: 100;
    font-size: 2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
    background-color: grey;
    width: 350px;
}

.third {
    position: absolute;
    top: 0%;
    left: 68%;
    font-size: 2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
    background-color: grey;
    width: 350px;
}

.fourth {
    position: absolute;
    top: 53%;
    z-index: 100;
    left: 3%;
    font-size: 2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
    background-color: grey;
    width: 350px;
}

.fifth {
    position: absolute;
    top: 53%;
    z-index: 100;
    left: 35%;
    font-size: 2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
    background-color: grey;
    width: 350px;
}

.sixth {
    position: absolute;
    top: 53%;
    left: 68%;
    font-size: 2vw;
    word-wrap: break-word;
    white-space: -moz-pre-wrap;
    white-space: pre-wrap;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<?php
    include('functions.php');
    readData();
    //echo json_encode($_SESSION['data']);
    $videos = getVideos();
    //echo $videos[0][0];
    if (empty($_SESSION['weight'])) {
        $_SESSION['weight'] = array();
    }
    
?>
<div class="row"> 
  <div class="column">
    <img tags="<?php echo $videos[0][2]; ?>" src="<?php echo $videos[0][0]; ?>" style="width:100%; height: 100%">
    <p class="first"> <?php echo wordwrap($videos[0][1],30,"\n"); ?> </p>
    <img tags="<?php echo $videos[3][2]; ?>" src="<?php echo $videos[3][0]; ?>" style="width:100%; height: 100%">
    <p class="fourth"> <?php echo wordwrap($videos[3][1],30,"\n"); ?> </p>
  </div>
  <div class="column">
    <img tags="<?php echo $videos[1][2]; ?>" src="<?php echo $videos[1][0]; ?>" style="width:100%; height: 100%">
    <p class="second"> <?php echo wordwrap($videos[1][1],30,"\n"); ?> </p>
    <img tags="<?php echo $videos[4][2]; ?>" src="<?php echo $videos[4][0]; ?>" style="width:100%; height: 100%">
    <p class="fifth"> <?php echo wordwrap($videos[4][1],30,"\n"); ?> </p>
  </div>  
  <div class="column">
    <img tags="<?php echo $videos[2][2]; ?>" src="<?php echo $videos[2][0]; ?>" style="width:100%; height: 100%">
    <p class="third"> <?php echo wordwrap($videos[2][1],30,"\n"); ?> </p>
    <img src="img/shuffle.png" style="width:100%; height: 100%">
  </div>
</div>

</body>
<script language="JavaScript">
    var d = new Date();

    $("img").on("click", function(e) {
        d = new Date();
      e.preventDefault();
      if (this.getAttribute("src") == "img/shuffle.png") {
          var json = {};
          $.ajax({
            type: "POST",
            url: "empty.php" ,
            data: json,
            success : function(res) { 
                location.reload();
            }
          });
      } else {
          var json = {"tags" : this.getAttribute("tags")};
          //alert("Will call another monitor to play video: \n https://youtu.be/" +  this.getAttribute("src").split("/")[4]);
          //alert(this.getAttribute("tags"));
          $.ajax({
            type: "POST",
            url: "reweigh.php" ,
            data: json,
            success : function(res) { 
                //alert(res);
                
                console.log(new Date()-d);
                d = new Date();
                var array = res.split("\n");
                var i;
                
                for (i = 0; i < 5; i++) { 
                    var title = array[i].split(',')[0].replace(new RegExp('"', 'g'), "").replace(new RegExp("\\[|\\]", 'g'), "").replace(new RegExp("\\\\", 'g'), "");
                    var url = "https://img.youtube.com/vi/" + array[i].split(',')[1].replace(new RegExp('"', 'g'), "").replace(new RegExp("\\[|\\]", 'g'), "").replace(new RegExp("\\\\", 'g'), "").split("/")[3] + "/0.jpg";
                    var tags = array[i].split(',')[2].replace(new RegExp('"', 'g'), "").replace(new RegExp("\\[|\\]", 'g'), "").replace(new RegExp("\\\\", 'g'), "");
                    $("img")[i].setAttribute("tags", tags);
                    $("img")[i].setAttribute("src", url);
                    $("img").next()[i].innerHTML = title;
                }
                
                //location.reload();

            }
          });
      }
      
      
    });

</script>
</html>
