<?php
session_start();
require('functions.php');

$weight = $_SESSION['weight'];
$data = $_SESSION['data'];

$tags = $_POST['tags'];
$array = explode("|", strtolower($tags));
foreach ($array as $a) {
	if (!in_array($a, $weight)) {
		array_push($weight, $a);
	}
}

$_SESSION['weight'] = $weight;
//echo json_encode($_SESSION['weight']);
$dataScores = [];
foreach ($data as $video) {
	$videoTags = $video[2];
	$splittedTags = explode("|", strtolower($videoTags));
	$score = 0;
	foreach ($splittedTags as $tag) {
		if (in_array($tag, $weight)) {
			$score = $score + 1;
		}
	}
	$dataScores[json_encode($video)] = $score;
}
arsort($dataScores);
$counter = 0;
foreach($dataScores as $v => $score) {
	if ($counter < 5) {
		echo $v;
		echo "\n";
		$counter = $counter + 1;
	} else {
		break;
	}
}
?>